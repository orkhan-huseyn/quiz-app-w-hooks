import React, { useState, useEffect } from 'react';
import Timer from './Timer';

import './App.css';

const API_URL = 'https://run.mocky.io/v3/8e20dcec-6106-45f5-a3b1-a3989911f1ee';

function App() {
  const [questions, setQuestions] = useState([]);
  const [numberOfCorrectAnswers, setCorrectAnswers] = useState(0);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [isQuizFinished, setQuizFinished] = useState(false);

  useEffect(() => {
    // when component is mounted (all React elements are rendered to DOM)
    // we make a GET request to the API and retrieve list of questions
    // then we store questions in out component state
    fetch(API_URL)
      .then((response) => response.json())
      .then((questions) => setQuestions(questions));
  }, []);

  function handleClick(answer) {
    // if clicked answer is a correct answer
    // then we need to increment numberOfCorrectAnswers by 1
    if (answer.isCorrect) {
      setCorrectAnswers((answers) => answers + 1);
    }

    // we need to update currentQuestionIndex, to go to the next question
    // we do so by storing nextQuestionIndex in a variable
    const nextQuestionIndex = currentQuestionIndex + 1;

    // we check if nextQuestionIndex is less than the length of our questions array
    // if yes, we can update the currentQuestionIndex to be next
    // else we will set quiz as finished by setting isQuizFinished to true
    if (nextQuestionIndex < questions.length) {
      setCurrentQuestionIndex(nextQuestionIndex);
    } else {
      setQuizFinished(true);
    }
  }

  // current question is a question at currentQuestionIndex
  const currentQuestion = questions[currentQuestionIndex];

  // if currentQuestion is undefined, it means our questions array is empty
  // which means that we have not fetched questions from API yet
  // so let's show dummy 'Loading...' text to users so they know what's happening
  if (!currentQuestion) {
    return (
      <div className="container">
        <h2>Loading questions...</h2>
      </div>
    );
  }

  // if isQuizFinished is true it means quiz is finished
  // we need to show quiz finished screen and also numberOfCorrectAnswers
  if (isQuizFinished) {
    // we can change className of the wrapper div conditionally
    // so we declare a variable called containerClassName
    let containerClassName = 'container';

    // if there is no correct answer we add 'container-red' class
    // and it will make background-color a reddish color (see App.css)
    if (numberOfCorrectAnswers === 0) {
      containerClassName += ' container-red';
    } else if (numberOfCorrectAnswers === questions) {
      // if all answers are correct we add 'container-green' class
      // so it will make background-color some green color
      containerClassName += ' container-green';
    } else {
      // if it's neither of them then background will be yellow
      // so we add 'container-yellow' to wrapper div
      containerClassName += ' container-yellow';
    }

    return (
      <div className={containerClassName}>
        <div>
          <h1>Quiz completed!</h1>
          <div>Number of correct answers is {numberOfCorrectAnswers}</div>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <Timer
        remaininingTimeInMinutes={1}
        onFinish={() => {
          // when timer is finished
          // Timer component will call onFinish callback that
          // we have pased to it
          // so time's up, we set quiz to finished
          setQuizFinished(true);
        }}
      />
      <div className="question">
        <h4>{currentQuestion.text}</h4>
        {currentQuestion.answers.map((answer, index) => (
          <button onClick={() => handleClick(answer)} key={index}>
            {answer.text}
          </button>
        ))}
      </div>
    </div>
  );
}

export default App;
