import React, { useState, useEffect } from 'react';

function Timer({ remaininingTimeInMinutes, onFinish }) {
  const [remainingMinutes, setRemainingMinutes] = useState(0);
  const [remainingSeconds, setRemainingSeconds] = useState(0);

  useEffect(() => {
    // calculate endTime by adding remaininingTimeInMinutes * 60 * 1000 milliseconds to current time
    const now = new Date().getTime();
    const remainingTimeInMilliseconds = remaininingTimeInMinutes * 60 * 1000;
    const endTime = now + remainingTimeInMilliseconds;

    // start the interval and save timerID in a variable
    // so that we can clearInterval laterƒ
    const timerID = setInterval(() => {
      // calculate distance to the timer end
      // by subtracting current time from endTime
      const now = new Date().getTime();
      const distance = endTime - now;

      // check if timer is finished
      // it's when distance is a negative number
      const isTimerFinished = distance < 0;
      if (isTimerFinished) {
        // if timer is done, then let's call onFinish callback
        // that has been passed to this component by props
        onFinish();
        // let's clear the interval if we're done with timer
        clearInterval(timerID);
        return;
      }

      // calculate remaining minutes and seconds from distance
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // then update minutes and seconds in component state
      setRemainingMinutes(minutes);
      setRemainingSeconds(seconds);
    }, 1000);

    return () => {
      // clear the initerval if component is unmounted
      // a.k.a removed from DOM to prevent memory leaks
      clearInterval(timerID);
    };
  }, []);

  // pad the intergers so that we can show remaining time
  // like 00:57 or 00:05 instead of 0:57 or 0:5
  const paddedMinutes = remainingMinutes.toString().padStart(2, '0');
  const paddedSeconds = remainingSeconds.toString().padStart(2, '0');

  return (
    <h5>
      {paddedMinutes}:{paddedSeconds}
    </h5>
  );
}

export default Timer;
